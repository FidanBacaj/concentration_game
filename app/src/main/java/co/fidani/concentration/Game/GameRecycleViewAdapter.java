package co.fidani.concentration.Game;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.fidani.concentration.Constants.Constants;
import co.fidani.concentration.Model.GameLogic;
import co.fidani.concentration.R;

/**
 * Created by fidan on 12/22/17.
 */

public class GameRecycleViewAdapter extends RecyclerView.Adapter<GameRecycleViewAdapter.ViewHolder> {
    public Context context;
    private List<GameLogic> gameLogicsList;
    private ClickListener clickListener;

    GameRecycleViewAdapter(Context context,List<GameLogic> gameLogicsList){

        this.context=context;
        this.gameLogicsList=gameLogicsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,null);

        GameRecycleViewAdapter.ViewHolder vh = new GameRecycleViewAdapter.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GameLogic gm = gameLogicsList.get(position);

        Glide.with(context)
                .load(Constants.getInstance().getPhotoUri(gm.getPhotoId(),gm.getPhotoSecret(),gm.getPhotoServer(),gm.getPhotoFarm()+""))
                .into(holder.imageViewBack);

        if (gm.isDone()) {
            holder.rootLayout.setVisibility(View.GONE);
        } else if (gm.isInvers()) {
            holder.flipCard(true);
            holder.rootLayout.setVisibility(View.VISIBLE);
        } else {
            holder.flipCard(false);
            holder.rootLayout.setVisibility(View.VISIBLE);
        }

        holder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gm.isDone()) {
                    return;
                }

                clickListener.itemClicked(position, holder);
            }
        });
    }


    @Override
    public int getItemCount() {
        return gameLogicsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setClickLisener(ClickListener clickLisener){
        this.clickListener = clickLisener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        final View rootLayout;
        final View cardFace;
        final View cardBack;
       ImageView imageViewBack;

        public ViewHolder(final View itemView) {
            super(itemView);

            rootLayout = (View) itemView.findViewById(R.id.card_root);
            cardFace = (View) itemView.findViewById(R.id.card_face);
            cardBack = (View) itemView.findViewById(R.id.card_back);
            imageViewBack = (ImageView)itemView.findViewById(R.id.img_back);
        }

        public void flipCard(boolean isInvers)
        {

            if (isInvers) {
                if (cardBack.getVisibility() == View.GONE) {
                    FlipAnimation flipAnimation = new FlipAnimation(cardBack, cardFace);
                    flipAnimation.reverse();
                    rootLayout.startAnimation(flipAnimation);
                }
            } else {
                if (cardFace.getVisibility() == View.GONE) {
                    FlipAnimation flipAnimation = new FlipAnimation(cardFace, cardBack);
                    flipAnimation.reverse();
                    rootLayout.startAnimation(flipAnimation);
                }
            }
        }
    }

    public  interface ClickListener{
        public void itemClicked(int position, ViewHolder holder);
    }


}
