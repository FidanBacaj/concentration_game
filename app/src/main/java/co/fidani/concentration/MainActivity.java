package co.fidani.concentration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import co.fidani.concentration.Constants.Constants;

public class MainActivity extends AppCompatActivity {

    private Button btnAvatar,btnSinglePlayer,btnMultiPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        btnAvatar = (Button)findViewById(R.id.btn_avatar);
        btnSinglePlayer = (Button)findViewById(R.id.btn_single_player);
        btnMultiPlayer = (Button)findViewById(R.id.btn_multi_player);



        Constants.getInstance().setAvatar(this,"dog");

        btnAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,AvatarActivity.class);

                startActivity(intent);
            }
        });

        btnSinglePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,LevelGameActivity.class);
                 intent.putExtra("modeGame",1);
                startActivity(intent);
            }
        });

        btnMultiPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,LevelGameActivity.class);
                intent.putExtra("modeGame",2);
                startActivity(intent);
            }
        });

    }
}
