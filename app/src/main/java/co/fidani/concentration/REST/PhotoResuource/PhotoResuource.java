package co.fidani.concentration.REST.PhotoResuource;

import com.google.gson.annotations.SerializedName;

import co.fidani.concentration.Model.Photos;

/**
 * Created by fidan on 12/22/17.
 */

public class PhotoResuource {
    @SerializedName("photos")
    private Photos photos;
    @SerializedName("stat")
    private String stat;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
