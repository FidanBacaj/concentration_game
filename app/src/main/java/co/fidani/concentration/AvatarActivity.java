package co.fidani.concentration;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import co.fidani.concentration.Constants.Constants;

public class AvatarActivity extends AppCompatActivity {
    private ImageButton btnBack;
    private RelativeLayout dog,cat,horse,chicken;
    private View selectDog,selectCat,selectHorse,selectChicken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_avatar);

        btnBack =(ImageButton)findViewById(R.id.btn_back);

        dog  =(RelativeLayout)findViewById(R.id.dog);
        cat = (RelativeLayout)findViewById(R.id.cat);
        horse = (RelativeLayout)findViewById(R.id.horse);
        chicken = (RelativeLayout)findViewById(R.id.chicken);

        selectDog = (View)findViewById(R.id.select_dog);
        selectCat = (View)findViewById(R.id.select_cat);
        selectHorse = (View)findViewById(R.id.select_horse);
        selectChicken = (View)findViewById(R.id.select_chicken);

        dog.setOnClickListener(wichCliced);
        cat.setOnClickListener(wichCliced);
        horse.setOnClickListener(wichCliced);
        chicken.setOnClickListener(wichCliced);

        firstSelect();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AvatarActivity.super.onBackPressed();
            }
        });






    }


    View.OnClickListener wichCliced = new View.OnClickListener() {
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.dog:
                    selectDog.setVisibility(View.VISIBLE);
                    selectCat.setVisibility(View.INVISIBLE);
                    selectHorse.setVisibility(View.INVISIBLE);
                    selectChicken.setVisibility(View.INVISIBLE);

                    Constants.getInstance().setAvatar(getApplicationContext(),"dog");
                    break;
                case R.id.cat:
                    selectDog.setVisibility(View.INVISIBLE);
                    selectCat.setVisibility(View.VISIBLE);
                    selectHorse.setVisibility(View.INVISIBLE);
                    selectChicken.setVisibility(View.INVISIBLE);
                    Constants.getInstance().setAvatar(getApplicationContext(),"cat");
                    break;
                case R.id.horse:
                    selectDog.setVisibility(View.INVISIBLE);
                    selectCat.setVisibility(View.INVISIBLE);
                    selectHorse.setVisibility(View.VISIBLE);
                    selectChicken.setVisibility(View.INVISIBLE);
                    Constants.getInstance().setAvatar(getApplicationContext(),"horse");
                    break;
                case R.id.chicken:
                    selectDog.setVisibility(View.INVISIBLE);
                    selectCat.setVisibility(View.INVISIBLE);
                    selectHorse.setVisibility(View.INVISIBLE);
                    selectChicken.setVisibility(View.VISIBLE);
                    Constants.getInstance().setAvatar(getApplicationContext(),"chicken");
                    break;
            }
        }
    };

    private void firstSelect() {
        switch (Constants.getInstance().getAvatar(getApplicationContext())){
            case "dog":
                selectDog.setVisibility(View.VISIBLE);
                break;

            case "cate":
                selectCat.setVisibility(View.VISIBLE);
                break;

            case "horse":
                selectHorse.setVisibility(View.VISIBLE);
                break;

            case "chicken":
                selectChicken.setVisibility(View.VISIBLE);
                break;
        }
    }
}
