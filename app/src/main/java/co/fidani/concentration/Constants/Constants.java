package co.fidani.concentration.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import co.fidani.concentration.Model.HighScore;
import io.realm.Realm;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by fidan on 12/22/17.
 */

public final class Constants {
    private static final Constants ourInstance = new Constants();

    private String FLICKER_API_KEY = "d92b3a2bb158f4215747a9f2e71263cf";
    private String FLICKER_SECRET = "74d5a868718650a2";

    public static Constants getInstance() {
        return ourInstance;
    }

    public void setAvatar(Context context,String avatar){

        SharedPreferences sharedPreferences = Constants.getInstance().getShared(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("avatar", avatar);
        editor.commit();
    }

    public String getAvatar(Context context){
        SharedPreferences shared = Constants.getInstance().getShared(context);
        return shared.getString("avatar",null);
    }

    public SharedPreferences getShared(Context context) {
        return context.getSharedPreferences("concetration",MODE_PRIVATE);
    }

    public Uri getPhotoUri(String photoId,String photoSecret,String photoServer,String photoFarm){

        return Uri.parse("https://farm"+photoFarm+".staticflickr.com/"+photoServer+"/"+photoId+"_"+photoSecret+".jpg");
    }

    public void setFirstTime(Context context,int firstime){

        SharedPreferences sharedPreferences = Constants.getInstance().getShared(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("firstime", firstime);
        editor.commit();


    }

    public int getFirstTime(Context context){
        SharedPreferences shared = Constants.getInstance().getShared(context);
        return shared.getInt("firstime",0);
    }

    public void setHightScore(int level,int score){
        HighScore highScore = new HighScore(level,score);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(highScore);
        realm.commitTransaction();
    }

    public int getHightScore(int level){
        Realm realm = Realm.getDefaultInstance();

        HighScore highScore = realm.where(HighScore.class).equalTo("id",level).findFirst();

        return highScore.getHighScore();
    }
}
