package co.fidani.concentration.Game;

import android.graphics.Rect;
import android.support.annotation.IntRange;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by fidan on 12/22/17.
 */

public class RecycleViewMargin extends RecyclerView.ItemDecoration {
    private final int columns;
    private int margin;

    public RecycleViewMargin(@IntRange(from=0)int margin , @IntRange(from=0) int columns ) {
        this.margin = margin;
        this.columns=columns;

    }


    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildLayoutPosition(view);

        outRect.right = margin;
        outRect.left= margin;

        outRect.bottom = margin;

        if(position%columns==0|| position%columns==1){
            outRect.top = margin;

        }



    }

}
