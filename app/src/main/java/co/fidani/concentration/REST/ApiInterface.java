package co.fidani.concentration.REST;

import android.support.annotation.Nullable;

import co.fidani.concentration.Constants.Constants;
import co.fidani.concentration.REST.PhotoResuource.PhotoResuource;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by fidan on 12/22/17.
 */

public interface ApiInterface {

    @GET("services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=d92b3a2bb158f4215747a9f2e71263cf")
    Call<PhotoResuource> getPhotos(@Query("per_page") String perPage
             , @Nullable @Query("tags") String tags);


}
