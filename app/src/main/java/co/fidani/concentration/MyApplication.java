package co.fidani.concentration;

import android.app.Application;
import android.util.Log;

import co.fidani.concentration.Constants.Constants;
import co.fidani.concentration.Model.HighScore;
import io.realm.Realm;

/**
 * Created by fidan on 12/23/17.
 */

public class MyApplication extends Application {

    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        if(Constants.getInstance().getFirstTime(this)==0){

            HighScore highScoreL1 = new HighScore(1,0);
            HighScore highScoreL2 = new HighScore(2,0);
            HighScore highScoreL3 = new HighScore(3,0);

            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(highScoreL1);
            realm.copyToRealmOrUpdate(highScoreL2);
            realm.copyToRealmOrUpdate(highScoreL3);
            realm.commitTransaction();

            Constants.getInstance().setFirstTime(this,1);
        }

    }
}
