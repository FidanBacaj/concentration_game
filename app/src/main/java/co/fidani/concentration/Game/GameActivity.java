package co.fidani.concentration.Game;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Delayed;

import co.fidani.concentration.Constants.Constants;
import co.fidani.concentration.Model.GameLogic;
import co.fidani.concentration.Model.Photo;
import co.fidani.concentration.R;
import co.fidani.concentration.REST.APIClient;
import co.fidani.concentration.REST.ApiInterface;
import co.fidani.concentration.REST.PhotoResuource.PhotoResuource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GameActivity extends AppCompatActivity implements GameRecycleViewAdapter.ClickListener {

    private RecyclerView recyclerViewGame;
    private GameRecycleViewAdapter adapter;
    private GridLayoutManager gm;
    private ImageButton btnBack;
    private TextView higheScore,p1,p2;
    private int LEVEL;
    AlertDialog.Builder mBuilder;
    View mview;
    private int gameMode;

    private  int turnCount = 0;
    private int p1Count=0;
    private int p2Count=0;
    private int turn =1;

    ApiInterface apiInterface;

    //logic game
    private List<GameLogic> gameLogicsList = new ArrayList<>();
    private int touchCount=0;
    private Integer selectedIndex1 = null;
    private Integer selectedIndex2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        LEVEL = getIntent().getIntExtra("level",0);
        gameMode = getIntent().getIntExtra("modeGame",0);

        btnBack = (ImageButton)findViewById(R.id.btn_back);
        recyclerViewGame = (RecyclerView)findViewById(R.id.game_recycleview);
        higheScore = (TextView)findViewById(R.id.txt_hight_score);
        p1 = (TextView)findViewById(R.id.pl1);
        p2 = (TextView)findViewById(R.id.pl2);

        p1.setBackgroundResource(R.drawable.button_corner);

        higheScore.setText("HighScore: "+Constants.getInstance().getHightScore(LEVEL-2));

        if (gameMode==2){
            higheScore.setVisibility(View.INVISIBLE);
        }else {
            p1.setVisibility(View.INVISIBLE);
            p2.setVisibility(View.INVISIBLE);

        }

        mainFunction();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancleAlert();
            }
        });
    }

    public void mainFunction(){
        Call call = apiInterface.getPhotos(LEVEL+"", Constants.getInstance().getAvatar(getApplicationContext()));

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                PhotoResuource pr = (PhotoResuource) response.body();

                if(pr.getStat().equals("ok")){
                    startGame(pr.getPhotos().getPhoto());
                }else{
                    Toast.makeText(getApplicationContext(),"Api_key expired",Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("D",t.toString());
            }
        });

    }

    public void startGame(List<Photo> photos){

        for (int i =0 ; i<=photos.size()-1;i++) {
            Photo photo = photos.get(i);

            GameLogic gameLogic = new GameLogic(photo.getId(),
                    photo.getSecret()
                    ,photo.getServer()
                    ,photo.getFarm()
                    ,false
                    ,false);

            gameLogicsList.add(gameLogic);
            gameLogicsList.add(gameLogic);

        }

        ShuffleList.shuffleList(gameLogicsList);

        RecycleViewMargin decorationList = new RecycleViewMargin(16, 1);
        recyclerViewGame.addItemDecoration(decorationList);
        gm = new GridLayoutManager(this,LEVEL);
        recyclerViewGame.setLayoutManager(gm);
        recyclerViewGame.setNestedScrollingEnabled(false);
        adapter = new GameRecycleViewAdapter(this,gameLogicsList);
        adapter.setClickLisener(GameActivity.this);
        recyclerViewGame.setAdapter(adapter);
    }

    @Override
    public void itemClicked(int position, final GameRecycleViewAdapter.ViewHolder holder) {

        if (selectedIndex1 == null) {
            selectedIndex1 = position;
            holder.flipCard(true);
            touchCount++;

        } else if (selectedIndex1 == position) {
            //no action
        } else if (selectedIndex2 == null) {
            selectedIndex2 = position;
            holder.flipCard(true);
            //check if match
            final GameActivity current = this;

            final Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (selectedIndex1 == null || selectedIndex2 == null) {
                        current.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                current.adapter.notifyDataSetChanged();
                                current.checkGameStatus();
                            }
                        });
                        return;
                    }

                    GameLogic first = gameLogicsList.get(selectedIndex1);
                    GameLogic second = gameLogicsList.get(selectedIndex2);

                    if (first.getPhotoId() == second.getPhotoId()) {
                        first.setDone(true);
                        second.setDone(true);

                        if(gameMode==2){

                            if (turn==1){
                                p1Count++;
                                current.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        p1.setText("P1: "+p1Count);

                                    }
                                });
                            }else {
                                p2Count++;
                                current.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        p2.setText("P2: "+p2Count);

                                    }
                                });
                            }
                        }

                    }else {

                        if(gameMode==2){

                            if (turn==1){
                                turn=2;

                                current.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        p1.setBackgroundColor(Color.TRANSPARENT);
                                        p2.setBackgroundResource(R.drawable.button_corner);

                                    }
                                });

                            }else {
                                turn=1;

                                current.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        p2.setBackgroundColor(Color.TRANSPARENT);
                                        p1.setBackgroundResource(R.drawable.button_corner);

                                    }
                                });
                            }
                        }


                    }

                    selectedIndex1 = null;
                    selectedIndex2 = null;
                    touchCount++;


                    current.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            current.adapter.notifyDataSetChanged();
                            current.checkGameStatus();
                        }
                    });

                    timer.cancel();
                    timer.purge();
                }
            }, 1000);

        } else if (selectedIndex2 == position) {
            selectedIndex2 = null;

            holder.flipCard(false);
        }
    }

    private void checkGameStatus() {

        Boolean finished = true;

        for (GameLogic gameLogic :
                gameLogicsList) {
            if (gameLogic.isDone() == false) {
                finished = false;
                break;
            }
        }

        if (finished) {
            Log.e("Finished:" , "Success"+touchCount);

            if (gameMode==1){

                resultAlert();

                if(Constants.getInstance().getHightScore(LEVEL-2)!=0){
                    if (touchCount<Constants.getInstance().getHightScore(LEVEL-2)){

                        Constants.getInstance().setHightScore(LEVEL-2,touchCount);
                    }
                }else {
                    Constants.getInstance().setHightScore(LEVEL-2,touchCount);
                }

            }else {
                multiResultAlert();
            }

        }
    }

    private void cancleAlert(){

        ImageButton btnCancel,btnChack;

        mBuilder = new AlertDialog.Builder(GameActivity.this);
        mview = getLayoutInflater().inflate(R.layout.canceled_view, null);
        mBuilder.setView(mview);

        btnCancel=(ImageButton)mview.findViewById(R.id.btn_cancel);
        btnChack=(ImageButton)mview.findViewById(R.id.btn_chack);

        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        btnChack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameActivity.super.onBackPressed();
            }
        });

    }

    private void resultAlert(){

        final ImageButton btnReload,btnNext;
        TextView point;

        mBuilder = new AlertDialog.Builder(GameActivity.this);
        mview = getLayoutInflater().inflate(R.layout.result_item, null);
        mBuilder.setView(mview);

        btnReload=(ImageButton)mview.findViewById(R.id.btn_reload);
        btnNext=(ImageButton)mview.findViewById(R.id.btn_next);
        point = (TextView)mview.findViewById(R.id.point);

        point.setText(touchCount+" Touch");

        if(LEVEL==5) {

            btnNext.setVisibility(View.INVISIBLE);
        }

        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                touchCount=0;
                selectedIndex1 = null;
                selectedIndex2 = null;
                higheScore.setText("HighScore: "+Constants.getInstance().getHightScore(LEVEL-2));
                gameLogicsList.clear();
                mainFunction();

                dialog.dismiss();

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LEVEL!=5){
                    LEVEL++;
                    touchCount=0;
                    selectedIndex1 = null;
                    selectedIndex2 = null;
                    higheScore.setText("HighScore: "+Constants.getInstance().getHightScore(LEVEL-2));
                    gameLogicsList.clear();
                    mainFunction();
                }

                dialog.dismiss();
            }
        });
    }

    private void multiResultAlert(){

        final ImageButton btnReload,btnNext;
        TextView p1f,p2f;

        mBuilder = new AlertDialog.Builder(GameActivity.this);
        mview = getLayoutInflater().inflate(R.layout.multi_player_result, null);
        mBuilder.setView(mview);

        btnReload=(ImageButton)mview.findViewById(R.id.btn_reload);
        btnNext=(ImageButton)mview.findViewById(R.id.btn_next);
        p1f = (TextView) mview.findViewById(R.id.p1f);
        p2f = (TextView) mview.findViewById(R.id.p2f);

        if (p1Count>p2Count){

            p1f.setText("P1-WINNER: "+p1Count);
            p1f.setTextColor(Color.GREEN);
            p2f.setText("P2-LOSER: "+p2Count);
            p2f.setTextColor(Color.RED);
        }else {

            p2f.setText("P2-WINNER: "+p2Count);
            p2f.setTextColor(Color.GREEN);
            p1f.setText("P1-LOSER: "+p1Count);
            p1f.setTextColor(Color.RED);
        }

        if(LEVEL==5) {
            btnNext.setVisibility(View.INVISIBLE);
        }

        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                touchCount=0;
                selectedIndex1 = null;
                selectedIndex2 = null;

                turn=1;
                p1Count=0;
                p2Count=0;
                p1.setText("P1:");
                p2.setText("P2:");
                p2.setBackgroundColor(Color.TRANSPARENT);
                p1.setBackgroundResource(R.drawable.button_corner);
                gameLogicsList.clear();
                mainFunction();

                dialog.dismiss();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LEVEL!=5){
                    LEVEL++;
                    touchCount=0;
                    turn=1;
                    p1Count=0;
                    p2Count=0;
                    p1.setText("P1:");
                    p2.setText("P2:");
                    p2.setBackgroundColor(Color.TRANSPARENT);
                    p1.setBackgroundResource(R.drawable.button_corner);
                    selectedIndex1 = null;
                    selectedIndex2 = null;
                    gameLogicsList.clear();
                    mainFunction();
                }

                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {

        cancleAlert();
    }
}
