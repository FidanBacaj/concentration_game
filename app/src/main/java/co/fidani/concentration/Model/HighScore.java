package co.fidani.concentration.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by fidan on 12/23/17.
 */

public class HighScore extends RealmObject {

    @PrimaryKey
    private int id;

    private int highScore;

    public HighScore(int id_level, int highScore) {
        this.id = id_level;
        this.highScore = highScore;
    }

    public HighScore() {
    }

    public int getId_level() {
        return id;
    }

    public void setId_level(int id_level) {
        this.id = id_level;
    }


    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }


}
