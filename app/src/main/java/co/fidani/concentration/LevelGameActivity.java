package co.fidani.concentration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import co.fidani.concentration.Constants.Constants;
import co.fidani.concentration.Game.GameActivity;

public class LevelGameActivity extends AppCompatActivity {
    private ImageButton btnBack;
    private ImageView lock2,lock3;
    private RelativeLayout star1,star2,star3;
    private View selectstar1,selectstar2,selectstar3;

    private  int gameMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_level_game);

        gameMode = getIntent().getIntExtra("modeGame",0);

        btnBack =(ImageButton)findViewById(R.id.btn_back);

        star1  =(RelativeLayout)findViewById(R.id.star1);
        star2 = (RelativeLayout)findViewById(R.id.star2);
        star3 = (RelativeLayout)findViewById(R.id.star3);

        selectstar1 = (View)findViewById(R.id.select_star1);
        selectstar2 = (View)findViewById(R.id.select_star2);
        selectstar3 = (View)findViewById(R.id.select_star3);

        lock2=(ImageView)findViewById(R.id.lock2);
        lock3=(ImageView)findViewById(R.id.lock3);

        if(gameMode==1){

            if (Constants.getInstance().getHightScore(1)==0){
                lock2.setVisibility(View.VISIBLE);
            }
            if (Constants.getInstance().getHightScore(2)==0){
                lock3.setVisibility(View.VISIBLE);
            }

        }

        star1.setOnClickListener(wichCliced);
        star2.setOnClickListener(wichCliced);
        star3.setOnClickListener(wichCliced);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelGameActivity.super.onBackPressed();
            }
        });



    }

    View.OnClickListener wichCliced = new View.OnClickListener() {
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.star1:
                    selectstar1.setVisibility(View.VISIBLE);
                    selectstar2.setVisibility(View.INVISIBLE);
                    selectstar3.setVisibility(View.INVISIBLE);

                    Intent Level1 = new Intent(LevelGameActivity.this, GameActivity.class);
                    Level1.putExtra("level",3);
                    Level1.putExtra("modeGame",gameMode);

                    startActivity(Level1);

                    break;
                case R.id.star2:
                    selectstar1.setVisibility(View.INVISIBLE);
                    selectstar2.setVisibility(View.VISIBLE);
                    selectstar3.setVisibility(View.INVISIBLE);

                    if (Constants.getInstance().getHightScore(1)!=0){
                        Intent Level2 = new Intent(LevelGameActivity.this, GameActivity.class);
                        Level2.putExtra("level",4);
                        Level2.putExtra("modeGame",gameMode);

                        startActivity(Level2);
                    }else {

                        Toast.makeText(LevelGameActivity.this,"Level 2 is locked",Toast.LENGTH_SHORT).show();
                    }


                    break;
                case R.id.star3:
                    selectstar1.setVisibility(View.INVISIBLE);
                    selectstar2.setVisibility(View.INVISIBLE);
                    selectstar3.setVisibility(View.VISIBLE);

                    if (Constants.getInstance().getHightScore(2)!=0){

                        Intent Level3 = new Intent(LevelGameActivity.this, GameActivity.class);
                        Level3.putExtra("level",5);
                        Level3.putExtra("modeGame",gameMode);

                        startActivity(Level3);
                    }else {

                        Toast.makeText(LevelGameActivity.this,"Level 3 is locked",Toast.LENGTH_SHORT).show();
                    }


                    break;

            }
        }
    };


}
