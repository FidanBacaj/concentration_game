package co.fidani.concentration.Model;

import android.support.annotation.NonNull;

/**
 * Created by fidan on 12/22/17.
 */

public class GameLogic {
    public int id;

    public String photoId;
    public String photoSecret;
    public String photoServer;
    public int photoFarm;
    public boolean invers = false;
    public boolean done = false;

    public GameLogic(String photoId, String photoSecret, String photoServer, int photoFarm, boolean invers, boolean done) {
        this.photoId = photoId;
        this.photoSecret = photoSecret;
        this.photoServer = photoServer;
        this.photoFarm = photoFarm;
        this.invers = invers;
        this.done = done;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getPhotoSecret() {
        return photoSecret;
    }

    public void setPhotoSecret(String photoSecret) {
        this.photoSecret = photoSecret;
    }

    public String getPhotoServer() {
        return photoServer;
    }

    public void setPhotoServer(String photoServer) {
        this.photoServer = photoServer;
    }

    public int getPhotoFarm() {
        return photoFarm;
    }

    public void setPhotoFarm(int photoFarm) {
        this.photoFarm = photoFarm;
    }

    public boolean isInvers() {
        return invers;
    }

    public void setInvers(boolean invers) {
        this.invers = invers;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }



}
