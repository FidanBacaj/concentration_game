package co.fidani.concentration.Game;

import java.util.List;
import java.util.Random;

import co.fidani.concentration.Model.GameLogic;

/**
 * Created by fidan on 12/22/17.
 */

public class ShuffleList {
    public static void shuffleList(List<GameLogic> a) {
        int n = a.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(a, i, change);
        }
    }

    private static void swap(List<GameLogic> a, int i, int change) {
        GameLogic helper = a.get(i);
        a.set(i, a.get(change));
        a.set(change, helper);
    }
}
